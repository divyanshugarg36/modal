import React from 'react';
import logo from './logo.svg';
import './style/index.scss';

import { Modal } from './common/Modal';
import { InputField } from './common/InputField';
import { SelectField } from './common/SelectField';
import { CheckField } from './common/CheckField';
import { RadioField } from './common/RadioField';

function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <Modal heading={'Edit Contact "Bruce Wayne"'}>
        <div className="contact-form">
          <div className="contact-form-col">
            <InputField label="First Name" />
            <InputField label="Last Name" />
          </div>
          <RadioField label="Gender" data={[{ title: 'Male', value: 'male' }, { title: 'Female', value: 'female' }, { title: 'Other', value: 'other' }]} />
          <InputField type="email" label="Email" />
          <div className="contact-form-col">
            <SelectField label="Department" data={[{ title: 'som', value: 'somethign' }]} />
            <InputField label="Contribution" />
          </div>
          <CheckField label="Is Active" />
        </div>
      </Modal>
    </div>
  );
}

export default App;
