import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';

import { CancelIcon } from '../icons';
import { Button } from './Button';

export const Modal = ({
  children,
  heading,
  className,
  loading,
  onClose,
  onSave,
}) => ReactDOM.createPortal(
  <div className={`modal ${className || ''}`} onClick={onClose}>
    <div className="modal-content" onClick={(e) => e.stopPropagation()}>
      <div className="modal-content-heading">
        <div className="modal-content-heading-text">{heading}</div>
        <CancelIcon className="close-icon" onClick={onClose} />
      </div>
      <div className="modal-content-body">{children}</div>
      <div className="modal-content-footer">
        <Button className="modal-content-footer-button" onClick={onSave}>Cancel</Button>
        <Button loading={loading} type="success" className="modal-content-footer-button" onClick={onSave}>Save</Button>
      </div>
    </div>
  </div>,
  document.body,
);

Modal.propTypes = {
  className: PropTypes.string,
  footerButtonText: PropTypes.string,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  loading: PropTypes.bool,
  heading: PropTypes.string,
};
