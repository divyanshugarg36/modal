import React from 'react';
import PropTypes from 'prop-types';
import { FormField } from './FormField';

export const InputField = ({
  className, id, label, type, value, onChange, placeholder,
}) => {
  const inputId = id || (Math.random() + 1).toString(36).substring(7);
  return (
    <FormField className={className} id={inputId} label={label}>
      <input id={inputId} className="form-field-input" type={type} onChange={onChange} value={value} placeholder={placeholder} />
    </FormField>
  );
};

InputField.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
};
InputField.defaultProps = {
  id: '',
  className: '',
  type: 'text',
  value: '',
  onChange: () => {},
  placeholder: '',
};
