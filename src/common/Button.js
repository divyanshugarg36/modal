import React from 'react';
import PropTypes from 'prop-types';
import { LoadingIcon } from '../icons';

export const Button = ({
  className, loading, children, buttonType, type, ...props
}) => (
  <button type={type} className={`primary-button ${buttonType ? `primary-button--${buttonType}` : ''} ${className || ''}`} {...props}>
    {loading ? (<LoadingIcon className="primary-button-icon" />) : (children)}
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool,
  buttonType: PropTypes.oneOf(['success']),
  type: PropTypes.string,
};
Button.defaultProps = {
  className: '',
  loading: false,
  buttonType: '',
  type: 'button',
};
