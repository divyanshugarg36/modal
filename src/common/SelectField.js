import React from 'react';
import PropTypes from 'prop-types';
import { FormField } from './FormField';

export const SelectField = ({
  className, id, label, type, value, onChange, placeholder, data,
}) => {
  const inputId = id || (Math.random() + 1).toString(36).substring(7);
  return (
    <FormField className={`${className} form-field--select`} id={inputId} label={label}>
      <select id={inputId} className="form-field-select" type={type} onChange={onChange} value={value} placeholder={placeholder}>
        {data?.map(({ title, value }) => (
          <option value={value}>{title}</option>
        ))}
      </select>
    </FormField>
  );
};

SelectField.propTypes = {
  className: PropTypes.string,
  data: PropTypes.instanceOf(Array),
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
};
SelectField.defaultProps = {
  id: '',
  className: '',
  type: 'text',
  data: [],
  value: '',
  onChange: () => {},
  placeholder: '',
};
