import React from 'react';
import PropTypes from 'prop-types';
import { CheckIcon } from '../icons';

export const CheckField = ({ label, value, onChange }) => (
  <div className="check-field" onClick={() => onChange(!value)}>
    <div className="check-field-box">
      {value ? <CheckIcon /> : ''}
    </div>
    <label className="check-field-label">{label}</label>
  </div>
);

CheckField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.bool,
  onChange: PropTypes.func,
};
CheckField.defaultProps = {
  value: false,
  onChange: () => {},
};
