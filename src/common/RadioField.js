import React from 'react';
import PropTypes from 'prop-types';
import { FormField } from './FormField';

export const RadioField = ({
  className, label, value: selected, onChange, data,
}) => (
  <FormField className={className} label={label}>
    <div className="form-field-radios">
      {data?.map(({ title, value }) => (
        <div className="form-field-radios-node" onChange={() => onChange(value)}>
          <div className={`form-field-radios-node-ring ${value === selected ? 'active' : ''}`} />
          <div className="form-field-radios-node-label">{title}</div>
        </div>
      ))}
    </div>
  </FormField>
);

RadioField.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  data: PropTypes.instanceOf(Array),
};
RadioField.defaultProps = {
  className: '',
  value: '',
  onChange: () => {},
  data: [],
};
