import React from 'react';
import PropTypes from 'prop-types';

export const FormField = ({
  className, id, label, children,
}) => (
  <div className={`form-field ${className}`}>
    <label className="form-field-label" htmlFor={id}>{`${label} :`}</label>
    {children}
  </div>
);

FormField.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
  id: PropTypes.string,
  label: PropTypes.string.isRequired,
};
FormField.defaultProps = {
  className: '',
  id: '',
};
